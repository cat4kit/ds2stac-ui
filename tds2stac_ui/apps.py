from django.apps import AppConfig


class Tds2StacUiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tds2stac_ui'
