from django.db import models
class tds2stac_ui(models.Model):
    class webservices(models.TextChoices):
        ncml = "ncml", "ncML"
        iso = "iso", "ISO"

    catalog=models.CharField(max_length=500)
    stac=models.BooleanField(default=False)
    stac_dir=models.CharField(max_length=500)
    stac_id=models.CharField(max_length=100)
    stac_description=models.CharField(max_length=500)
    web_service=models.CharField(
        max_length=200,
        choices=webservices.choices,
        default="iso",
    )
    stac_catalog_dynamic=models.BooleanField(default=False)
    datetime_filter_first=models.DateTimeField()
    datetime_filter_end=models.DateTimeField()
