from django.shortcuts import render
from .models import tds2stac_ui
import sys
from .tds2stac.tds2stac import app
from datetime import datetime

def tds2stac_django(request):
    if request.method=="POST":
        post=tds2stac_ui()
        post.catalog=request.POST['catalog']
        post.stac=request.POST['stac'].capitalize()
        post.stac_dir=request.POST['stac_dir']
        post.stac_id=request.POST['stac_id']
        post.stac_description=request.POST['stac_description']
        post.web_service=request.POST['web_service']
        post.stac_catalog_dynamic=request.POST['stac_catalog_dynamic'].capitalize()
        
        fisrt = datetime.strptime(request.POST['datetime_filter_first'], '%m/%d/%Y')
        post.datetime_filter_first=fisrt
        end = datetime.strptime(request.POST['datetime_filter_end'], '%m/%d/%Y')
        post.datetime_filter_end = end
        print(post.stac_catalog_dynamic)
        post.save()

        converter = app.Harvester(post.catalog,
                     stac=post.stac, stac_dir=post.stac_dir,
                     stac_id =post.stac_dir,
                     stac_description = post.stac_dir,
                     web_service=post.web_service,
                     stac_catalog_dynamic=post.stac_catalog_dynamic,
                    datetime_filter=[fisrt.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),end.strftime('%Y-%m-%dT%H:%M:%S.%fZ')])

        return render(request, 'tds2stac.html')
    else:
        return render(request, 'tds2stac.html')